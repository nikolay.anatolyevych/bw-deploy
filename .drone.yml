---
kind: pipeline
type: docker
name: default

steps:
- name: Launch an instance
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    # Check env for deployment. Use only dev/stg/prod in .deploy-env
    - env=$(cat .deploy-env)
    # Prepare inventory for remote SSH connections
    - cp -r ansible/inventories/remote-$env ansible/inventories/run
    # Prepare inventory for local Drone-Ansible run
    - sed -i "s/change_me/$env/g" ansible/inventories/localhost/group_vars/all
    # Launch
    - ansible-playbook -v -i ansible/inventories/localhost ansible/01_launch.yaml

- name: Set hostname
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    # Check IP and instance ID from previous step
    - IP=$(cat ansible/ip)
    - instance_id=$(cat ansible/id)
    - echo $instance_id
    - sed -i "s/change_me/$IP/g" ansible/inventories/run/hosts
    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/02_hostname.yaml

# Disabled by Marko until production launch
#- name: Install Graylog
#  image: mlerota/awscli_and_ansible:2.11.6_v4
#  commands:
#    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/03_graylog.yaml

- name: Code build - install NVM & download repo
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/04.1_build_code.yaml

- name: Code build - yarn install
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/04.2_build_code.yaml

- name: Code build - install bundler and run bundle install
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/04.3_build_code.yaml

- name: Code build - final step (precompile assets)
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/04.4_build_code.yaml

- name: ELB health check fix
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/05_elb_check_fix.yaml

- name: Configure Sidekiq
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/06_sidekiq.yaml

- name: Configure Puma
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/07_puma.yaml

- name: Install Nginx
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/08_nginx.yaml

#- name: Configure IDS
#  image: mlerota/awscli_and_ansible:2.11.6_v4
#  commands:
#    - ep=$(aws ssm get-parameter --name /prod/elastic_pass --region eu-west-1 --query 'Parameter.Value' --output text)
#    - sed -i "s/change_me_ep/$ep/g" ansible/roles/09_ids/templates/filebeat.yml.j2
#    - sed -i "s/change_me_ep/$ep/g" ansible/roles/09_ids/templates/auditbeat.yml.j2
#    - sed -i "s/change_me_ep/$ep/g" ansible/roles/09_ids/templates/metricbeat.yml.j2
#    - ansible-playbook -v --private-key ansible/build.pem -i ansible/inventories/run/hosts ansible/09_ids.yaml

- name: Create AMI
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - env=$(cat .deploy-env)
    - instance_id=$(cat ansible/id)
    - sed -i "s/change_me/$instance_id/g" ansible/roles/10_create_ami/tasks/main.yaml
    - sed -i "s/change_ami/$DRONE_BUILD_NUMBER/g" ansible/roles/10_create_ami/tasks/main.yaml
    - sed -i "s/change_env/$env/g" ansible/roles/10_create_ami/tasks/main.yaml
    - ansible-playbook -vv -i ansible/inventories/localhost ansible/10_create_ami.yaml

- name: Create preprod LC and update ASG
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - env=$(cat .deploy-env)
    - cp -r ansible/inventories/remote-$env ansible/inventories/run
    - sed -i "s/change_me/$DRONE_BUILD_NUMBER/g" ansible/roles/11_deploy_to_preprod/tasks/main.yaml
    - ansible-playbook -v -i ansible/inventories/run ansible/11_deploy_to_preprod.yaml

- name: Test preprod
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -i ansible/inventories/run ansible/12_pre-prod-test.yaml

- name: Downscale preprod
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - sed -i "s/change_me/$DRONE_BUILD_NUMBER/g" ansible/roles/13_downscale_preprod/tasks/main.yaml
    - ansible-playbook -v -i ansible/inventories/run ansible/13_downscale.yaml

- name: Create Live Launch Config and update ASG
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - sed -i "s/change_me/$DRONE_BUILD_NUMBER/g" ansible/roles/14_deploy_to_live/tasks/main.yaml
    - ansible-playbook -v -i ansible/inventories/run ansible/14_deploy_to_live.yaml

- name: Test live website
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -i ansible/inventories/run ansible/15_prod-test.yaml

- name: Update param store
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    - ansible-playbook -v -i ansible/inventories/localhost ansible/16_parm_update.yaml

- name: Terminate on end or failure
  image: mlerota/awscli_and_ansible:2.11.6_v4
  commands:
    # Check instance ID created in first step
    - instance_id=$(cat ansible/id)
    - sed -i "s/change_me/$instance_id/g" ansible/roles/terminate/tasks/main.yaml
    - ansible-playbook -vv -i ansible/inventories/localhost ansible/terminate.yaml
  when:
    status:
    - failure
    - success


# Check deploy on push
trigger:
  event:
  - custom
